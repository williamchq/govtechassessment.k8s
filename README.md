
/*docker build the application*/
docker build -t sample-web-content --build-arg firstname=William .

/*spin up application service and mysql service*/
kubectl apply -k ./ 
kubectl delete -k ./ 

/*Connect to mysql server from application pod*/
mysql --port=3306 --host=10.104.164.243 --user=root --password=pass1234 
mysqladmin --port=3306 --host=10.104.164.243 --user=root --password=pass1234 create 'test_db'
CREATE TABLE `test_db`.`new_table` (
  `id` INT NOT NULL,
  `value` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
insert into new_table values(1, "test");
select * from new_table;

/*Use Prometheus and Grafana to monitor resources*/
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install prometheus bitnami/kube-prometheus

kubectl port-forward --namespace default svc/prometheus-kube-prometheus-prometheus 9090:9090

helm install grafana bitnami/grafana
echo "$(kubectl get secret grafana-admin --namespace default -o jsonpath="{.data.GF_SECURITY_ADMIN_PASSWORD}" | base64 --decode)"

